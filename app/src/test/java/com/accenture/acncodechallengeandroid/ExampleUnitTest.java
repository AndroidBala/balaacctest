package com.accenture.acncodechallengeandroid;

import com.accenture.acncodechallengeandroid.model.ImgAlbum;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void getImgTitle() throws Exception {
        ImgAlbum imga = new ImgAlbum();
        imga.setTitle("HELLO");
        assertEquals(imga.getTitle(), "HELLO");
    }


}