package com.accenture.acncodechallengeandroid.api;

import com.accenture.acncodechallengeandroid.model.ImgAlbum;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PhotosAPI {
    @GET("photos/")
    Call<List<ImgAlbum>> getPhotos();
}
