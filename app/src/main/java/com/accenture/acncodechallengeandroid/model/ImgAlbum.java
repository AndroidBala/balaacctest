package com.accenture.acncodechallengeandroid.model;

public class ImgAlbum {
    private String title;
    private String url;
    private String thumbnailUrl;
    private int albumId;

    public ImgAlbum() {

    }

    public ImgAlbum(String title, String url, int albumId) {
        this.title = title;
        this.url = url;
        this.albumId = albumId;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public int getAlbumId() {
        return this.albumId;
    }
    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
