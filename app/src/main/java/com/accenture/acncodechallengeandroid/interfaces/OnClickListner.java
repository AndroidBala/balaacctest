package com.accenture.acncodechallengeandroid.interfaces;


public interface OnClickListner {
    void OnClick(String message, int position);

    void OnLongClick(String message, int position);
}