package com.accenture.acncodechallengeandroid.adapter

import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.accenture.acncodechallengeandroid.R
import com.accenture.acncodechallengeandroid.activity.MainActivity
import com.accenture.acncodechallengeandroid.app.GlideApp
import com.accenture.acncodechallengeandroid.interfaces.OnClickListner
import com.accenture.acncodechallengeandroid.model.ImgAlbum
import com.accenture.acncodechallengeandroid.util.AppLog
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.android.synthetic.main.list_row.view.*

class MyItemsAdapter(var activity: MainActivity, var thisList: ArrayList<ImgAlbum>, var listner : OnClickListner) : RecyclerView.Adapter<MyItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyItemsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyItemsAdapter.ViewHolder, position: Int) {
        holder.bindItems(position)
    }

    override fun getItemCount(): Int {
        AppLog().d("===========>>>>>. "+thisList.size)
        return thisList.size
    }

    fun getItem(location: Int): ImgAlbum? {
        return thisList.get(location)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(position: Int) {

            val item = getItem(position)!!

            if(!item.title.isNullOrBlank()) {
                itemView.title.title.setText(item.title)
            }else{
                itemView.title.title.setText(activity.resources.getString(R.string.no_title))
            }

            // item.albumId is primitive type, so it wont null
            itemView.albumId.setText(Integer.toString(item.albumId))

            if(!item.thumbnailUrl.isNullOrBlank()) {
                GlideApp.with(activity)
                        .load(item.thumbnailUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .fitCenter()
                        .error(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(itemView.thumbnail)
            }else{
                itemView.thumbnail.setImageResource(R.mipmap.ic_launcher)
            }

            itemView.setOnClickListener {
                if(!TextUtils.isEmpty(item.title))
                    listner.OnClick(item.title,position)
                else
                    listner.OnClick(activity.resources.getString(R.string.no_value),position)
            }

        }

    }

    fun addData(myData: ArrayList<ImgAlbum>){
        if(!myData.isEmpty()){
            thisList.clear()
            thisList.addAll(myData)
            notifyDataSetChanged()
        }
    }

}