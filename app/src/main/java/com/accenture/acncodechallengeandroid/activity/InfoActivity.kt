package com.accenture.acncodechallengeandroid.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.accenture.acncodechallengeandroid.R

class InfoActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#1b1b1b")))
        supportActionBar!!.title = getString(R.string.info)
    }

}