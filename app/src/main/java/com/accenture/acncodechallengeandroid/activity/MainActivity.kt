package com.accenture.acncodechallengeandroid.activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import com.accenture.acncodechallengeandroid.R
import com.accenture.acncodechallengeandroid.adapter.MyItemsAdapter
import com.accenture.acncodechallengeandroid.api.PhotosAPI
import com.accenture.acncodechallengeandroid.interfaces.OnClickListner
import com.accenture.acncodechallengeandroid.model.ImgAlbum
import com.accenture.acncodechallengeandroid.util.AppLog
import com.accenture.acncodechallengeandroid.util.CommonMethods
import com.accenture.acncodechallengeandroid.util.Constants
import com.accenture.acncodechallengeandroid.views.MyRecyclerView
import com.google.gson.GsonBuilder
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class MainActivity   : AppCompatActivity(), OnClickListner{

    internal var tts                  : TextToSpeech? = null
    private lateinit var pDialog      : KProgressHUD
    private var imgAlbumList          : List<ImgAlbum> = ArrayList()
    private lateinit var adapter      : MyItemsAdapter
    private lateinit var recyclerView : MyRecyclerView
    private lateinit var fb_info      : FloatingActionButton
    private lateinit var swipeRefresh : SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        fb_info      = findViewById(R.id.fb_info)
        recyclerView = findViewById<MyRecyclerView>(R.id.list)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        recyclerView.setEmptyView(findViewById<View>(R.id.tv_empty))

        // changing action bar color
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#1b1b1b")))

        //set layout manager for recyclerview
        recyclerView.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerView.setLayoutManager(mLayoutManager)

        //create adapter and set to recyclerview
        adapter = MyItemsAdapter(this@MainActivity, imgAlbumList as ArrayList<ImgAlbum>, this@MainActivity)
        recyclerView.setAdapter(adapter)

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                when(newState){
                    RecyclerView.SCROLL_STATE_IDLE -> fb_info.show()
                    else -> fb_info.hide()
                }
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

        fb_info.setOnClickListener {
            startActivity(Intent(this@MainActivity,InfoActivity::class.java).apply { Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK })
        }

        //initially load the data
        swipeRefresh.post {
            getDataFromApi(1)
            swipeRefresh.isRefreshing = false
        }

        //pull to refresh the data again
        swipeRefresh.setOnRefreshListener {
            getDataFromApi(2)
        }

    }

    /**
     * read data from api
     */
    private fun getDataFromApi(type:Int){
        //Check internet connection
        if(CommonMethods.getCommonMethods().isNetworkAvailable(applicationContext)){
            //create progress and show
            if(type==1) {
                pDialog = CommonMethods.getCommonMethods().createHUD(this@MainActivity)
            }
            loadData()
        }else{
            CommonMethods.getCommonMethods().ShowToast(applicationContext,getString(R.string.no_internet),1)
            swipeRefresh.isRefreshing = false
            return
        }
    }

    /**
     * Load data from base url
     */
    private fun loadData(){

        //https://jsonplaceholder.typicode.com/photos
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        val photosAPI = retrofit.create(PhotosAPI::class.java)

        val call = photosAPI.photos

        call.enqueue(object : Callback<List<ImgAlbum>> {
            override fun onResponse(call: Call<List<ImgAlbum>>, response: Response<List<ImgAlbum>>) {
                try {
                    if (response != null) {
                        hidePDialog()
                        imgAlbumList = response.body()!!
                        adapter.addData(imgAlbumList as ArrayList<ImgAlbum>)
                    } else {
                        CommonMethods.getCommonMethods().ShowToast(applicationContext, getString(R.string.cant_load), 1)
                    }
                }catch (ex:Exception){AppLog().d(ex.localizedMessage)}
            }

            override fun onFailure(call: Call<List<ImgAlbum>>, t: Throwable) {
                runOnUiThread {
                    CommonMethods.getCommonMethods().ShowToast(applicationContext, t.localizedMessage,1)
                    hidePDialog()
                }
            }
        })

    }

    /**
     * init textToSpeech listener
     */
    private fun initTTS(msg: String) {
        tts = TextToSpeech(this@MainActivity, TextToSpeech.OnInitListener { status ->
            if (status == TextToSpeech.SUCCESS) {
                val result = tts!!.setLanguage(Locale.US)
                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    AppLog().d( "This Language is not supported")
                } else {
                    ConvertTextToSpeech(msg)
                }
            } else
                AppLog().d("Initilization Failed!")
        })
    }

    /**
     * stop the TTS
     */
    override fun onPause() {
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }
        super.onPause()
    }

    public override fun onDestroy() {
        super.onDestroy()
        hidePDialog()
    }

    /**
     * handle the progressbar
     */
    private fun hidePDialog() {
        if (::pDialog.isInitialized && pDialog != null) {
            CommonMethods.getCommonMethods().cancelHUD(pDialog)
        }
        swipeRefresh.isRefreshing = false
    }

    /**
     * convert text to speech using default API
     */
    private fun ConvertTextToSpeech(msg: String?) {
        var msg = msg

        if (msg == null || "" == msg) {
            msg = getString(R.string.no_value)
            tts!!.speak(msg, TextToSpeech.QUEUE_FLUSH, null)
        } else
            tts!!.speak(msg, TextToSpeech.QUEUE_FLUSH, null)
    }

    /**
     * handle item click on recyclerview
     */
    override fun OnClick(message: String, position: Int) {
        if (!TextUtils.isEmpty(message)) {
            initTTS(message)
        } else
            initTTS(getString(R.string.no_value))
    }

    /**
     * handle long click on recyclerview item
     */
    override fun OnLongClick(message: String, position: Int) {
        if (!TextUtils.isEmpty(message)) {
            initTTS(message)
        } else
            initTTS(getString(R.string.no_value))
    }

}