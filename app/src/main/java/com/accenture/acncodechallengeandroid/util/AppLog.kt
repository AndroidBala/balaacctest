package com.accenture.acncodechallengeandroid.util

import android.text.TextUtils
import android.util.Log

class AppLog{
    fun d(msg:String){
        if(Constants.isTestBuild && !TextUtils.isEmpty(msg)){
            Log.d("MyAppLog => "," : " + msg)
        }
    }
}