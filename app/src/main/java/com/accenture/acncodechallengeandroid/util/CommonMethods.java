package com.accenture.acncodechallengeandroid.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.accenture.acncodechallengeandroid.R;
import com.kaopiz.kprogresshud.KProgressHUD;

public class CommonMethods {

    private static  CommonMethods commonMethods;

    public static CommonMethods getCommonMethods(){
        if (commonMethods==null) {
            commonMethods = new CommonMethods();
        }

        return commonMethods;
    }

    /**
     * Customized Toast msg
     * @param context
     * @param msg
     * @param duration
     */
    public void ShowToast(Context context, String msg, int duration){
        if(msg==null || context==null){
            return;
        }

        Toast toast = Toast.makeText(context, msg, duration);
        //toast.setGravity(Gravity.CENTER| Gravity.CENTER_HORIZONTAL, 0, 0);

        View view = toast.getView();
        view.getBackground().setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        TextView text = view.findViewById(android.R.id.message);
        text.setTextColor(context.getResources().getColor(R.color.list_row_start_color));

        toast.show();

    }

    /**
     * Create and return HUD to activity
     * @param activity
     * @return
     */
    public KProgressHUD createHUD(Activity activity){
        KProgressHUD kProgressHUD = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(activity.getResources().getString(R.string.please_wait))
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        return kProgressHUD;
    }

    /**
     * close the HUD
     * @param kProgressHUD
     */
    public void cancelHUD(KProgressHUD kProgressHUD){
        if(kProgressHUD!=null && kProgressHUD.isShowing()) {
            kProgressHUD.dismiss();
        }
    }

    /***
     * check internet is available
     * @param context
     * @return
     */
    public boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isFailover())
                return false;
            else if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected())
                return true;
            else
                return false;
        } catch (Exception e) {  }
        return true;
    }

}
