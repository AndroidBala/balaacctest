package com.accenture.acncodechallengeandroid.util

class Constants{

    companion object {
        const val isTestBuild     = false
        const val baseUrl         = "https://jsonplaceholder.typicode.com/"
    }

}